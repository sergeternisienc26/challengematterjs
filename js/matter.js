//import export commandes Matterjs

export const Engine = Matter.Engine;
export const Render = Matter.Render;
export const Runner = Matter.Runner;
export const Composites = Matter.Composites;
export const Events = Matter.Events;
export const Common = Matter.Common;
export const Composite = Matter.Composite;
export const Bodies = Matter.Bodies;
export const Body = Matter.Body;
export const MouseConstraint = Matter.MouseConstraint;
export const Mouse = Matter.Mouse;
export const world = Matter.World;

export let engine = Engine.create(); //création moteur 
export let render = Render.create({ //création moteur de rendu
  element: document.body,
  engine: engine,
  options: {
      wireframes:false, //false afin de rentrée options perso
      width: 1400,
      height: 680,
      background: 'rgb(255, 47, 218)',
    }
});

Engine.run(engine); //lancemenent moteur
Render.run(render); //lancement moteur de rendu
