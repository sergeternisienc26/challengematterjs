import { Bodies, Composite, engine } from './matter.js';

export function initPaddle(){
    let Paddle = Bodies.rectangle(700,660,120,22, { isStatic: true});
    Paddle.label = "Paddle";
   
    Composite.add(engine.world,[Paddle]);
};