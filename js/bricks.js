import { Bodies, Composite, engine } from './matter.js';

//variables 1 brick
let widthBrick = 120; //largeur brique
let heightBrick = 22; //hauteur brique
let posXBrick = 25; //position x init brick - position ligne(gauche)
let posYbrick = 10; //position y init brick - position colonne(haut)

//variable tableau briques
let countRowBrick = 12; //nombre de colonnes
let countColumnBrick = 2; //nombre de lignes
let padding = 20; //goulotte espace entre briques
//position brique dans tableau
let tabBricks = [];
for(let rowB=0; rowB<countRowBrick; rowB++) {
  tabBricks[rowB] = [];
  for(let colB=0; colB<countColumnBrick; colB++) {
    tabBricks[rowB][colB] = { x: 0, y: 0 };
  }
}



export function initBrick(){
        for(let colB=0; colB<countColumnBrick; colB++) {   //itération colonne tableau briques
        for(let rowB=0; rowB<countRowBrick; rowB++) { 
            let brickX = (rowB*(widthBrick)+posXBrick); //position colonne et espace gauche
            let brickY = (colB*(heightBrick)+posYbrick); //position ligne et espace haut
            tabBricks[rowB][colB].x = brickX;
            tabBricks[rowB][colB].y = brickY;
      let Brick = Bodies.rectangle(brickX+padding,brickY+padding,widthBrick-(2*padding),heightBrick-(2*padding), { isStatic: true});
    Brick.label = "Brick";
   
    Composite.add(engine.world,[Brick]);
        }
    }
};


